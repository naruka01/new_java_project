
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CalculateBill")
public class CalculateBill extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static double cost = 5;
	public static double cost1 = 6;
	public static double cost2 = 8;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int MNumber = Integer.parseInt(request.getParameter("MNumber"));
		String month = request.getParameter("month");
		int ucon = Integer.parseInt(request.getParameter("ucon"));
		
		float cost = 5;
		float cost1 = 6;
		float cost2 = 8;
		float bill;
		float GST;
		float MR = 200;
		float STax;
		float total;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:8082/electricity", "root",
					"Tiwari@2001");
			Statement st = conn.createStatement();
			st.executeUpdate(
					"insert into customer(mnumber,month,ucon)values('" + MNumber + "','" + month + "','" + ucon + "')");
			ResultSet rs = null;
			rs=st.executeQuery("select * from newusers");
			rs = st.executeQuery("select * from newusers where mnumber="+ MNumber);
			while (rs.next()) {
				String hR = "<html>";
				PrintWriter out = response.getWriter();
				response.setContentType("text/html");
				out.println("<html><body>");
				out.println("<link rel='icon' type='image/logo' href='EI.jpg'>");
				String fname = rs.getString("fname");
				String lname = rs.getString("lname");
				String address = rs.getString("address");
				String state = rs.getString("state");
				String city = rs.getString("city");
				String email = rs.getString("email");
				String mob = rs.getString("mob");

				if (ucon <= 100) {

					bill = ucon * cost;
					STax = (bill * 5) / 100;
					GST = (bill * 8) / 100;
					total = bill + STax + GST+MR;
					hR += "<div style='background-color: teal;font-family: monospace;width: 100%;max-width: 25%;margin: 20px auto; border-radius: 25px;'><center><h1 style='padding:20px;'>SBPDCL</h1><h2>BILL<h2><h3>"
							+ "Electricity Bill for the month of " + month + "</h3><h3>Name : " + fname + "" + " " + lname
							+ "</h3><h3>Address : " + address + "</h3><h3>State : " + state + "</h3><h3>City : " + city
							+ "</h3><h3>Email : " + email + "</h3><h3>Mobile Number: " + mob + "</h3><h4>Meter Rent : "
							+ MR + "</h4><h4>Unit Consumed : " + ucon + "</h4><h4>Total Charged : " + bill
							+ "</h4><h4>Service Tax : " + STax + "</h4><h4>GST : " + GST
							+ "</h4><h2 style='padding:15px;'>Total Payable = " + total + "</h2> </center></div>";

				} else if (ucon >= 100 && ucon <= 200) {
					bill = ucon * cost1;
					STax = (bill * 6) / 100;
					GST = (bill * 10) / 100;
					total = bill + STax + GST+MR;
					hR += "<div style='background-color: teal;font-family: monospace;width: 100%;max-width: 25%;margin: 20px auto; border-radius: 25px;'><center><h1 style='padding:20px;'>SBPDCL</h1><h2>BILL<h2><h3>"
							+ "Electricity Bill for the month of " + month + "</h3><h3>Name : " + fname + "" + " " + lname
							+ "</h3><h3>Address : " + address + "</h3><h3>State : " + state + "</h3><h3>City : " + city
							+ "</h3><h3>Email : " + email + "</h3><h3>Mobile Number: " + mob + "</h3><h4>Meter Rent : "
							+ MR + "</h4><h4>Unit Consumed : " + ucon + "</h4><h4>Total Charged : " + bill
							+ "</h4><h4>Service Tax : " + STax + "</h4><h4>GST : " + GST
							+ "</h4><h2 style='padding:15px;'>Total Payable = " + total + "</h2> </center></div>";
				} else if (ucon >= 200) {
					bill = ucon * cost2;
					STax = (bill * 8) / 100;
					GST = (bill * 12) / 100;
					total = bill + STax + GST+MR;

					hR += "<div style='background-color: teal;font-family: monospace;width: 100%;max-width: 25%;margin: 20px auto; border-radius: 25px;'><center><h1 style='padding:20px;'>SBPDCL</h1><h2>BILL<h2><h3>"
							+ "Electricity Bill for the month of " + month + "</h3><h3>Name : " + fname + "" + " " + lname
							+ "</h3><h3>Address : " + address + "</h3><h3>State : " + state + "</h3><h3>City : " + city
							+ "</h3><h3>Email : " + email + "</h3><h3>Mobile Number: " + mob + "</h3><h4>Meter Rent : "
							+ MR + "</h4><h4>Unit Consumed : " + ucon + "</h4><h4>Total Charged : " + bill
							+ "</h4><h4>Service Tax : " + STax + "</h4><h4>GST : " + GST
							+ "</h4><h2 style='padding:15px;'>Total Payable = " + total + "</h2> </center></div>";
				}

				conn.close();
				hR += "</html>";
				out.println(hR);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
