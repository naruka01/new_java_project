<%@ 
page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Audiowide">
<link rel="stylesheet" href="style.css">
<link rel="icon" type="image/logo" href="EI.jpg">
<meta charset="ISO-8859-1">
<title>SBPDCL</title>
</head>
<body>

	<!-- Navigation Bar -->

	<nav class="navbar navbar-inverse" style="margin-bottom: 0px;">
		<div class="container" style="width: 100%;">
			<div class="navbar-header">
				<a class="navbar-brand" style="font-size: 40px;"><b>JVVNL</b></a><br>
				<br>
				<h4 style="color: white; font-size: 25px;">Jaipur Vidyut Vitran
					Nigam Limited</h4>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a data-toggle="modal" data-target="#mymodal"
					style="font-size: 25px; padding: 30px;"><span
						class="glyphicon glyphicon-user"></span><b>Register</b></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right" style="margin-right: -35px;">
				<li><a data-toggle="modal" data-target="#myModal"
					style="font-size: 25px; padding: 30px;"><span
						class="glyphicon glyphicon-user"></span><b
						style="font-size: 22px;">Pay Bill</b></a></li>
			</ul>
		</div>
	</nav>

	<div id="carousel-example-generic"
		class="carousel slide container-fluid" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-generic" data-slide-to="0"
				class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			<li data-target="#carousel-example-generic" data-slide-to="3"></li>
			<li data-target="#carousel-example-generic" data-slide-to="4"></li>
			<li data-target="#carousel-example-generic" data-slide-to="5"></li>
		</ol>

		<div class="container">
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="i1.jpg" alt="..."
						style="width: 950px; height: 600px; margin: 1px auto; border-radius: 15px;">

				</div>
				<div class="item">
					<img src="i2.jpg" alt="..."
						style="width: 950px; height: 600px; margin: 1px auto; border-radius: 15px;">

				</div>
				<div class="item">
					<img src="i3.jpg" alt="..."
						style="width: 950px; height: 600px; margin: 1px auto; border-radius: 15px;">

				</div>
				<div class="item">
					<img src="i4.jpg" alt="..."
						style="width: 950px; height: 600px; margin: 1px auto; border-radius: 15px;">

				</div>
				<div class="item">
					<img src="i5.jpg" alt="..."
						style="width: 950px; height: 600px; margin: 1px auto; border-radius: 15px;">

				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic"
				role="button" data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#carousel-example-generic"
				role="button" data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

		<!-- New User Registration -->

		<div class="container">
			<div class="modal fade" id="mymodal" role="dialog">
				<div class="modal-dialog" style="width: 100%; max-width: 45%;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h1 style="text-align: center;">New User</h1>
						</div>
						<div class="modal-body">



							<div class="row align-items-center">
								<div class="col-md-3" style="width: 35%;">
									<img src="user.jpg"
										style="width: 100%; border-radius: 25px; margin: 163px auto;">
								</div>


								<div class="col-md-9" style="width: 65%;">
									<form action="Ncustomer.jsp">

										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="fname">First Name</label>
											</div>
											<div class="col-8">
												<input type="text" id="fname" name="fname"
													placeholder="Your name..">
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="lname">Last Name</label>
											</div>
											<div class="col-8">
												<input type="text" id="lname" name="lname"
													placeholder="Your last name..">
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="address">Address</label>
											</div>
											<div class="col-8">
												<input type="text" id="address" name="address"
													placeholder="Your Address..">
											</div>
										</div>

										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="state">State</label>
											</div>
											<div class="col-8">
												<input type="text" id="state" name="state"
													placeholder="Your State..">
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="city">City</label>
											</div>
											<div class="col-8">
												<input type="text" id="city" name="city"
													placeholder="Your City..">
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="email">Email</label>
											</div>
											<div class="col-8">
												<input type="email" id="email" name="email"
													placeholder="Your Email..">
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 20px; align-items: center;">
											<div class="col-4">
												<label for="mob">Mobile No.</label>
											</div>
											<div class="col-8">
												<input type="tel" id="mob" name="mob"
													placeholder="Your Mobile No...">
											</div>
										</div>

										<div class="row" style="padding: 5px; margin: 0px 70px;">
											<input type="submit" value="Submit">
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!-- To Pay Bill -->
		<div class="container">
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog" style="width: 100%; max-width: 40%;">
					<div class="modal-content"
						style="width: 100%; max-width: 70%; margin: 0px auto;">
						<div class="modal-header" style="padding: 0px;">
							<button type="button" class="close" data-dismiss="modal"
								style="padding: 10px;">&times;</button>
							<h1 style="text-align: center;">Bill</h1>
						</div>
						<div class="modal-body">
							<div class="row align-items-center">
								<div class="col-md-3" style="width: 50%;">
									<img src="calc.com.png" style="width: 95%; margin: 65px auto;">
								</div>
								<div class="col-md-9" style="width: 45%; margin-left: -25px;">
									<form action="CalculateBill">
										<div class="row"
											style="margin-bottom: 50px; align-items: center;">

											<label for="MNumber">Meter Number</label> <input type="text"
												id="MNumber" name="MNumber"
												placeholder="Your meter number...">

										</div>
										<div class="row"
											style="margin-bottom: 50px; align-items: center;">

											<label for="month">Month</label> <select id="month"
												name="month" style="border-radius: 12px;">
												<!-- <option value="">Please choose your gender...</option> -->
												<option value="January">January</option>
												<option value="February">February</option>
												<option value="March">March</option>
												<option value="April">April</option>
												<option value="May">May</option>
												<option value="June">June</option>
												<option value="July">July</option>
												<option value="August">August</option>
												<option value="September">September</option>
												<option value="October">October</option>
												<option value="November">November</option>
												<option value="December">December</option>

											</select>
										</div>

										<div class="row"
											style="margin-bottom: 50px; align-items: center;">

											<label for="ucon">Unit Consumed</label>

											<div class="col-8">
												<input type="text" id="ucon" name="ucon"
													placeholder="Unit Consumed...">
											</div>
										</div>
										<div class="row">
											<input type="submit" value="Submit">
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p>
		Jaipur Vidyut Vitran Nigam Limited(Jaipur Discom) is engaged in
		distribution and supply of electricity in 12 districts of Rajasthan,
		Namely Jaipur, Dausa, Alwar, Bharatpur, Dholpur, Kota, Bundi,
		Baran,jhalawar, Sawaimadhopur, Tonk and Karauli.(Except Kota &
		Bharatpur City). JVVNL has an infrastructure facility in its operating
		area with <b>512</b> power system stabilizers, <b>705</b> power
		transformers, <b>139 33</b> kV feeders, <b>825 11</b> kV feeders and
		around<b> 29,668</b> distribution transformers of various capacities.
	</p>

	<!-- Style -->
	<style>
p {
	font-family: "Audiowide", sans-serif;
	padding: 0px 20px 20px 20px;
	margin: 0px 20px 20px 20px;
	font-size: 20px;
	line-height: 2.0;
}

* {
	box-sizing: border-box;
}

input[type=text], select, textarea {
	width: 100%;
	max-width: 95%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
}

label {
	font-size: 20px;
	font-family: "Audiowide", sans-serif;
}

input[type=email], select, textarea {
	width: 100%;
	max-width: 95%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
}

input[type=tel], select, textarea {
	width: 100%;
	max-width: 95%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
}

label {
	display: inline-block;
}

input[type=submit] {
	background-color: black;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 10px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: blue;
}

.container {
	border-radius: 5px;
}

h1 {
	font-family: "Audiowide", sans-serif;
}

/* Clear floats after the columns */
.row:after {
	content: "";
	clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
	.col-25, .col-75, input[type=submit] {
		width: 100%;
		margin-top: 0;
	}
}

.modal-content {
	font-family: "Audiowide", sans-serif;
}
</style>
</body>
</html>