<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<head>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Audiowide">
<link rel="stylesheet" href="style.css">
<link rel="icon" type="image/logo" href="EI.jpg">


<meta charset="ISO-8859-1">
<title>SBPDCL</title>
</head>
<body>

	<div class="inner-box"
		style="width: 100%; max-width: 40%; text-align: center; background-color: #1FCCBF; height: 500px; margin: 10px auto 0; border-radius: 25px; box-shadow: -31px 0px 20px 0px;">

		<div class="container">
			<div class="modal fade" id="mymodal" role="dialog">
				<div class="modal-dialog" style="width: 100%; max-width: 45%;">
					<div class="modal-content"
						style="width: 100%; max-width: 80%; margin: 0px auto;">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h1 style="text-align: center;">New User</h1>
						</div>
						<div class="modal-body">
							<div class="row align-items-center">
								<div class="col-md-3">
									<img src="calc.com.png"
										style="width: 160%; height: 250px; margin-left: -35px; margin-bottom: 65px;">
								</div>
								<div class="col-md-9">
									<form action="CalculateBill">
										<div class="row"
											style="margin-bottom: 50px; align-items: center;">
											<div class="col-4">
												<label for="mnumber">Meter Number</label>
											</div>
											<div class="col-8">
												<input type="text" id="mnumber" name="mnumber"
													placeholder="Your meter number...">
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 50px; align-items: center;">
											<div class="col-4">
												<label for="month">Month</label>
											</div>
											<div class="col-8">
												<select id="month" name="month" style="border-radius: 12px;">
													<!-- <option value="">Please choose your gender...</option> -->
													<option value="Jan">January</option>
													<option value="Feb">February</option>
													<option value="March">March</option>
													<option value="April">April</option>
													<option value="May">May</option>
													<option value="June">June</option>
													<option value="July">July</option>
													<option value="Aug">August</option>
													<option value="Sep">September</option>
													<option value="Oct">October</option>
													<option value="Nov">November</option>
													<option value="Dec">December</option>

												</select>
											</div>
										</div>
										<div class="row"
											style="margin-bottom: 50px; align-items: center;">
											<div class="col-4">
												<label for="ucon">Unit Consumed</label>
											</div>
											<div class="col-8">
												<input type="text" id="ucon" name="ucon"
													placeholder="Unit Consumed...">
											</div>
										</div>
										<div class="row"
											style="padding: 15px; margin: 0px 135px; margin-top: 15px;">
											<input type="submit" value="Submit">
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
<style>
* {
	box-sizing: border-box;
}

select {
	
}

input[type=text], select, textarea {
	width: 100%;
	max-width: 95%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 15px;
	resize: vertical;
}

label {
	font-size: 20px;
	font-family: "Audiowide", sans-serif;
}

input[type=email], select, textarea {
	width: 100%;
	max-width: 95%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
}

input[type=tel], select, textarea {
	width: 100%;
	max-width: 95%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	resize: vertical;
}

label {
	display: inline-block;
}

input[type=submit] {
	background-color: black;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 10px;
	cursor: pointer;
}

input[type=submit]:hover {
	background-color: blue;
}

.container {
	border-radius: 5px;
}

h1 {
	font-family: "Audiowide", sans-serif;
	margin: 25px 0;
	padding-top: 20px;
}

/* Clear floats after the columns */
.row:after {
	content: "";
	clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
	.col-25, .col-75, input[type=submit] {
		width: 100%;
		margin-top: 0;
	}
}
</style>
</html>